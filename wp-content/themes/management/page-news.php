<?php 

/**
* Template Name: News Page
*
* @package Qubaxis
*/

?>
<?php get_header(); ?>

		<!-- Page Title Section -->
		<div class="row page-title page-title-about" style="background: url(<?php echo get_upload_uri() . '/2018/05/page-title-about.jpg'; ?>) !important ">
			<div class="container">
				<h2><i class="fa fa-graduation-cap"></i>ABOUT US</h2>
			</div>
		</div>
		

		
		<!-- What we offer section -->
		<div class="row section-row">
			<div class="container">
				<div class="section-row-header-center">
					<h6><i class="fa fa-star-o"></i>WE ARE BEST<i class="fa fa-star-o"></i></h6>
					<h1>LATEST NEWS</h1>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="news-item-container">
						<div class="news-img">
							<img src="<?php echo get_upload_uri() . '/2018/05/news-sm1.jpg'; ?>" alt="news"/>
							<div class="news-item-date">
								<h6><i class="fa fa-calendar"></i>08, AUG</h6>
							</div>
						</div>
						<div class="news-item">
							<h5>NEW ACADEMIC CALENDER</h5>
							<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
							<a href="#"><i class="fa fa-paper-plane"></i>VIEW DETAILS</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="news-item-container">
						<div class="news-img">
							<img src="<?php echo get_upload_uri() . '/2018/05/news-sm2.jpg'; ?>" alt="news"/>
							<div class="news-item-date">
								<h6><i class="fa fa-calendar"></i>13, JUL</h6>
							</div>
						</div>
						<div class="news-item">
							<h5>EXAM RESULTS OUT</h5>
							<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
							<a href="#"><i class="fa fa-paper-plane"></i>VIEW DETAILS</a>
						</div>
					</div>
				</div>
				<div class="clearfix visible-sm-block"></div>
				<div class="col-sm-6 col-md-4">
					<div class="news-item-container">
						<div class="news-img">
							<img src="<?php echo get_upload_uri() . '/2018/05/news-sm3.jpg'; ?>" alt="news"/>
							<div class="news-item-date">
								<h6><i class="fa fa-calendar"></i>11, JUN</h6>
							</div>
						</div>
						<div class="news-item">
							<h5>EXAM DATES</h5>
							<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
							<a href="#"><i class="fa fa-paper-plane"></i>VIEW DETAILS</a>
						</div>
					</div>
				</div>
				<div class="clearfix visible-lg visible-md visible-sm"></div>
				<div class="view-all-link">
					<a href="#"><i class="fa fa-paper-plane"></i>VIEW ALL</a>
				</div>
			</div>
		</div>
		
		
	

<?php get_footer(); ?>