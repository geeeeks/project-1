<?php 

/**
* Template Name: Admission Page
*
* @package Qubaxis
*/

?>
<?php get_header(); ?>

    <!-- Page Title Section -->
		<div class="row page-title page-title-about" style="background: url(<?php echo get_upload_uri() . '/2018/05/page-title-about.jpg'; ?>) !important ">
			<div class="container">
				<h2><i class="fa fa-graduation-cap"></i>ADMISSIONS</h2>
			</div>
		</div>
		
		<!-- Facts section -->
        <div class="row section-row">
            <div class="container">
				<div class="fact-wrapper">
					<div class="col-md-5 fact-item">
						<p class="lg-icon"><i class="fa fa-trophy"></i></p>
						<p>PATHSHALA won best School award in 2016</p>
						<h1>WINNER BEST SCHOOL AWARD</h1>
						<p>It is a long established fact that a reader will be distracted by the content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the content.</p>
					</div>
					<div class="col-md-7 fact-item">
						<div class="fact-item-list">
							<div class="col-xs-3">
								<i class="fa fa-star"></i>
							</div>
							<div class="col-xs-9">
								<h2>14+ <br />COMPETITION WON</h2>
								<p>It is a long established fact that a reader will be distracted.</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="fact-item-list">
							<div class="col-xs-3">
								<i class="fa fa-clock-o"></i>
							</div>
							<div class="col-xs-9">
								<h2>1000+ <br />VOLUNTEER HOURS</h2>
								<p>It is a long established fact that a reader will be distracted.</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="fact-item-list">
							<div class="col-xs-3">
								<i class="fa fa-graduation-cap"></i>
							</div>
							<div class="col-xs-9">
								<h2>100% <br />SUCCESS RATE</h2>
								<p>It is a long established fact that a reader will be distracted.</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
        </div>
		
		<!-- Numbers section -->
		<div class="row number-row">
			<div class="container">
				<div class="col-sm-6 col-md-3 number-item">
					<i class="fa fa-book"></i>
					<span>30+</span>
					<p>COURSES OFFERED</p>
				</div>
				<div class="col-sm-6 col-md-3 number-item">
					<i class="fa fa-graduation-cap"></i>
					<span>20</span>
					<p>QUALIFIED TEACHERS</p>
				</div>
				<div class="col-sm-6 col-md-3 number-item">
					<i class="fa fa-child"></i>
					<span>300</span>
					<p>ENERGETIC STUDENTS</p>
				</div>
				<div class="col-sm-6 col-md-3 number-item">
					<i class="fa fa-clock-o"></i>
					<span>100+</span>
					<p>AWESOME EVENTS</p>
				</div>
			</div>
		</div>
		
		<!-- Teachers row -->
		<div class="row section-row">
			<div class="container">
				<div class="section-row-header-center">
					<h6><i class="fa fa-star-o"></i>WE ARE BEST<i class="fa fa-star-o"></i></h6>
					<h1>WHY PATHSHALA?</h1>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
				</div>
				<div class="col-md-6">
					<div id="pathshalaAccordion" class="pathshala-accordion faq-accordion">
						<h4 class="accordion-header">Why you should choose Pathshala?</h4>
						<div class="accordion-inner">
							<p>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
									ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
									amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi.
							</p>
						</div>
						<h4 class="accordion-header">What Pathshala Offers?</h4>
						<div class="accordion-inner">
							<p>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
									ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
									amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi.
							</p>
						</div>
						<h4 class="accordion-header">How Pathshala is different?</h4>
						<div class="accordion-inner">
							<p>Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
									ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
									amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi.
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="admissioon-youtube-video">
						<iframe src="https://www.youtube.com/embed/054bszkOk_Y" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		
		<!-- Know More Info & Admission apply row -->
		<div class="row apply-know-row">
			<div class="container">
				<div class="col-sm-6 admission-row">
					<h3>ADMISSIONS ARE OPEN</h3>
					<p>It is a long established fact that a reader will be distracted by the content of a page when looking at its layout.</p>
					<a href="#"><i class="fa fa-edit"></i>APPLY NOW</a>
				</div>
				<div class="col-sm-6 info-row">
					<h3>HAVE ANY QUERIES?</h3>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
					<div class="input-wrapper">
						<input type="text" placeholder="e.g. email@pathshala.com"/><a href="#"><i class="fa fa-paper-plane"></i></a>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Footer Section -->
		<div class="row footer-row">
			<div class="container">
				<div class="school-logo">
					<i class="fa fa-graduation-cap"></i>
					<h3>PATHSHALA</h3>
					<h6>BETTER WAY TO LEARN & GROW</h6>
				</div>
				<div class="col-md-4 col-sm-6 footer-item">
					<h5>CONTACT US</h5>
					<p><i class="fa fa-map-marker"></i>3768 Seabury Ct, Burlington, NC, 27215</p>
					<p><i class="fa fa-mobile"></i> +1 8910000891</p>
					<p><i class="fa fa-envelope"></i>email@pathshala.com</p>
				</div>
				<div class="col-md-2 col-sm-6 footer-item">
					<h5>QUICK LINKS</h5>
					<div class="quick-link-box">
						<a href="#"><i class="fa fa-graduation-cap"></i>ACADEMICS</a>
						<a href="#"><i class="fa fa-users"></i>ADMISSION</a>
						<a href="#"><i class="fa fa-calendar"></i>EVENTS</a>
						<a href="#"><i class="fa fa-thumbs-up"></i>CAMPUS LIFE</a>
					</div>
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-md-3 col-sm-6 footer-item">
					<h5>SCHOOL TIMINGS</h5>
					<p><i class="fa fa-clock-o"></i> MON - FRI 9:00 AM - 3:00 PM</p>
					<p><i class="fa fa-clock-o"></i> SAT 9:00 AM - 1:00 PM</p>
				</div>
				<div class="col-md-3 col-sm-6 footer-item">
					<h5>SUBSCRIBE</h5>
					<div class="footer-subscribe">
						<i class="fa fa-envelope"></i></a><input type="text" placeholder="example@pathshala.com" />
					</div>
					<a href="#" class="subscribe-link"><i class="fa fa-paper-plane"></i>SUBMIT</a>
				</div>
			</div>
			<div class="footer-social-row">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-google-plus"></i></a>
				<a href="#"><i class="fa fa-linkedin"></i></a>
			</div>
		</div>
		
		
		<!-- Login Modal -->
		<!-- Modal -->
		<div class="modal fade" id="loginModal" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content login-modal">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><i class="fa fa-sign-in"></i>LOGIN</h4>
					</div>
					<div class="modal-body">
						<div>
							<label><i class="fa fa-user"></i>USERNAME/EMAIL</label>
							<input class="form-control" type="text" placeholder="Username/Email">
						</div>
						<div>
							<label><i class="fa fa-key"></i>PASSWORD</label>
							<input class="form-control" type="password" placeholder="Password">
						</div>
						<a href="#" class="forgot-link">FORGOT PASSWORD?</a>
						<a href="#" class="login-link"><i class="fa fa-sign-in"></i>LOGIN</a>
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>